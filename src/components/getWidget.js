import React, { Fragment } from "react"
import getWidgets from "./getWidgets"
import { areas } from "react-pe-layouts"
import { __ } from "./i18n" 

export function initArea(areaName, data = {}, defArea = null) {
  // console.log( getWidgets )
  // console.log( areaName, areas()[ areaName ] )
  if (!areas()[areaName]) return defArea
  const ts = areas()[areaName].area
  //console.log(areaName, areas()[ areaName ] );
  if (ts && Array.isArray(ts) && ts.length > 0) {
    const rr = ts.map((widgetAreaData, i) => {
      if (!getWidgets[widgetAreaData.component]) {
        console.log(`component need in «${areaName}» and «${widgetAreaData.component}» not exist.`)
        return <div className="title" key={i}>
          {__(widgetAreaData.title)}
        </div>
      }
      const ElWidget = getWidgets[widgetAreaData.component].default
      const title = widgetAreaData.title && widgetAreaData.title !== ""
        ? (
          <div className="title">
            {__(widgetAreaData.title)}
          </div>
        )
        : null
      // console.log( data );
      return (
        <Fragment key={i}>
          {title}
          <ElWidget {...widgetAreaData} {...data} defArea={defArea} />
        </Fragment>
      )
    })
    return rr
  }

  return defArea
}
export function initDataArea(areaName, data={}, defaultData=null)
{
  
}
export function widgetAreas() {
  return areas()
}
export default function getWidget(widget, templatePart) {
  const ts = areas()[templatePart]
  if (ts && Array.isArray(ts)) {
    const w = ts.filter((e) => e.component === widget)
    if (!w[0]) return null
    if (!getWidgets[w[0].component]) return null
    const ElWidget = getWidgets[w[0].component].default
    return <ElWidget {...w[0]} />
  }

  return null
}

export function initWidget(widget, data = {}) {
  const ElWidget = getWidgets[widget].default
  return <ElWidget {...data} />
}
