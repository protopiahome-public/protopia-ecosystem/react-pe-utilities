import $ from "jquery";
export default function scrollToElement(elementY, duration = 1000) {
  const startingY = window.pageYOffset;
  const diff = elementY - startingY;
  let start; // Bootstrap our animation - it will get called right before next frame shall be rendered.

  window.requestAnimationFrame(function step(timestamp) {
    if (!start) start = timestamp; // Elapsed milliseconds since start of scrolling.

    const time = timestamp - start; // Get percent of completion in range [0, 1].

    const percent = Math.min(time / duration, 1);
    window.scrollTo(0, startingY + diff * percent); // Proceed with animation as long as we wanted it to.

    if (time < duration) {
      window.requestAnimationFrame(step);
    }
  });
}
export function scrollToElementByJQuery(element, duration = 1000, shiftY = 0) {
  // window.scrollTo({
  //   top: $(element).offset().top - shiftY,
  //   behavior: "smooth"
  // });
  const startingY = window.pageYOffset;

  const diff = () => {
    return $(element).offset().top - shiftY - startingY;
  };

  let start; // Bootstrap our animation - it will get called right before next frame shall be rendered.

  window.requestAnimationFrame(function step(timestamp) {
    if (!start) {
      start = timestamp;
    } // Elapsed milliseconds since start of scrolling.


    const time = timestamp - start; // Get percent of completion in range [0, 1].

    const percent = Math.min(time / duration, 1); //console.log( parseInt(startingY + diff() * percent)/ 10 )

    window.scrollTo(0, startingY + diff() * percent); // Proceed with animation as long as we wanted it to.

    if (time < duration) {
      window.requestAnimationFrame(step);
    }
  });
}