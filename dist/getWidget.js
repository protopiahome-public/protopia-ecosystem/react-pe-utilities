function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Fragment } from "react";
import getWidgets from "./getWidgets";
import { areas } from "react-pe-layouts";
import { __ } from "./i18n";
export function initArea(areaName, data = {}, defArea = null) {
  // console.log( getWidgets )
  // console.log( areaName, areas()[ areaName ] )
  if (!areas()[areaName]) return defArea;
  const ts = areas()[areaName].area; //console.log(areaName, areas()[ areaName ] );

  if (ts && Array.isArray(ts) && ts.length > 0) {
    const rr = ts.map((widgetAreaData, i) => {
      if (!getWidgets[widgetAreaData.component]) {
        console.log(`component need in «${areaName}» and «${widgetAreaData.component}» not exist.`);
        return /*#__PURE__*/React.createElement("div", {
          className: "title",
          key: i
        }, __(widgetAreaData.title));
      }

      const ElWidget = getWidgets[widgetAreaData.component].default;
      const title = widgetAreaData.title && widgetAreaData.title !== "" ? /*#__PURE__*/React.createElement("div", {
        className: "title"
      }, __(widgetAreaData.title)) : null; // console.log( data );

      return /*#__PURE__*/React.createElement(Fragment, {
        key: i
      }, title, /*#__PURE__*/React.createElement(ElWidget, _extends({}, widgetAreaData, data, {
        defArea: defArea
      })));
    });
    return rr;
  }

  return defArea;
}
export function initDataArea(areaName, data = {}, defaultData = null) {}
export function widgetAreas() {
  return areas();
}
export default function getWidget(widget, templatePart) {
  const ts = areas()[templatePart];

  if (ts && Array.isArray(ts)) {
    const w = ts.filter(e => e.component === widget);
    if (!w[0]) return null;
    if (!getWidgets[w[0].component]) return null;
    const ElWidget = getWidgets[w[0].component].default;
    return /*#__PURE__*/React.createElement(ElWidget, w[0]);
  }

  return null;
}
export function initWidget(widget, data = {}) {
  const ElWidget = getWidgets[widget].default;
  return /*#__PURE__*/React.createElement(ElWidget, data);
}